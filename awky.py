#!/usr/bin/env python3
import re
import sys
import argparse
import textwrap
from multiprocessing import Pool, cpu_count

def match(cre, string, template=None):
    m=cre.search(string)
    if m==None:
        #no match, don't do anything
        return None
    if template==None:
        #no template, just return string to print
        return string.strip()
    #retrun the expanded string
    return m.expand(template)

#multiprocessor helper function, using globals
def match_mp(line):
    global cre
    global args
    return  match(cre, line, args.format)

def lines(fname=None):
    # Generate lines from stdin or file
    if fname==None:
        for line in sys.stdin:
           yield line
    else:
        with open(fname, 'rt') as f:
            for line in f:
                yield line

if __name__ == '__main__':

    #construct command line parser
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,

        description=textwrap.dedent('''\
            Match regex to input and format output based on matches
            -------------------------------------------------------
                - REGEX takes the form of a python regex
                - FORMAT uses \\1, \\2 to replace groups,
                  or \g<name>, \g<1>, \g<2> to match named groups
        '''),

        epilog="echo 'number 234' | ./awky -r '(?P<value>\d+)' -f '\g<value>'"
    )

    #input file
    parser.add_argument('-i', dest='ifile', required=False, default=None,
        help="Read from input file rather than stdin",
    )

    #regex pattern
    parser.add_argument('-r', dest='regex', required=True,
        help="Regex pattern",
    )

    #output format
    parser.add_argument('-f', '--fmt', dest='format', required=False, default=None,
        help="Output format string. When not passed whole matching lines are returned",
    )

    #parse arguments
    args = parser.parse_args()

    #compile the regex
    cre = re.compile(args.regex)

    #create a pool of workers
    workers = Pool(cpu_count()*2)


    #for each line in the input search the regex and perform expansion
    for s in workers.imap(match_mp, lines(args.ifile), chunksize=128):
        if s!=None: print(s)
