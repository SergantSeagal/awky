AWKY
----
Awky (pronounce Awkeye) is a simplified awk version based on python which
implements one of the most common uses of awk: "selecting parts of inputs
strings and formatting it into a new output"

Awky leverages python's multiprocessing module to speed up processing.
Despite this its performance might be less than a pure C implementation would
provide, but for most cases this simple script will suffice.

Installation
------------
Awky is a simple python script, and can be directly called from the
command line.
No installation of external tools, other than python3, is required.

Usage
-----
Awky reads directly from stdin, or a file when specified using the ```-i```
flag.

Using the ```-r``` flag a regex can be specified.
Python's named groups can be used, i.e., (?P\<name\>expression).

If no format flag ```-f``` is passed, awky behaves like grep, only printing
those input lines which match the regex.
When ```-f``` is passed, it will use this format string to reformat each
matching line.
Python's match group identifiers, i.e., \1, \2, etc. can be used in the
format string, as well as named group identifiers, i.e., \g\<name\>, \g\<1\>, etc.

Example
------
Strip ip addresses from ```ip a``` output.
```ip a | ./awky.py -r "(?P<ip>(\d{1,3}\.){3}\d{1,3})" -f 'address: \g<ip>'```

